// Class Car
class Car {
  constructor(cars) {
    this.cars = cars;
  }
  filterCarByUser() {
    let driver = document.getElementById("driver").value;
    let date = document.getElementById("date").value;
    let time = document.getElementById("time").value;
    let dateTime = date + time;
    let totalPassanger = document.getElementById("passanger").value;

    //validate data
    if (driver === undefined || driver === "") {
      alert("Pengisian data tidak komplit");
      return;
    } else if (dateTime < getDateTimeNow()) {
      alert("Pilih tanggal h-1 penyewaan");
    } else if (totalPassanger == "" && driver != "") {
      return this.cars.filter(
        (car) => car.available === true && car.availableAt <= dateTime
      );
    } else if (totalPassanger != "" && driver != "") {
      return this.cars.filter(
        (car) =>
          car.available === true &&
          car.capacity >= totalPassanger &&
          car.availableAt <= dateTime
      );
    }
  }
}

// Module Request
let xmlHttp = new XMLHttpRequest();
xmlHttp.open("GET", "http://localhost:8000/api/cars", false);
xmlHttp.send(null);
let data = JSON.parse(xmlHttp.responseText);

// create new Car Object
let cars = new Car(data);

// Get Element by ID carsList
let app = document.getElementById("carsList");
htmlData = "";

// Function Format Rupiah
function formatToRupiah(number) {
  return new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
  }).format(number);
}

// add onClickListener to filter button
let btnFilterCar = document
  .getElementById("cariMobil")
  .addEventListener("click", getCars);

// Fungsi untunk Mendapatkan waktu
function getDateTimeNow() {
  let today = new Date();
  let date =
    today.getFullYear() +
    "-" +
    String(today.getMonth() + 1).padStart(2, "0") +
    "-" +
    String(today.getDate()).padStart(2, "0");
  let time =
    String(today.getHours()).padStart(2, "0") +
    ":" +
    String(today.getMinutes()).padStart(2, "0") +
    ":" +
    String(today.getSeconds()).padStart(2, "0");
  let dateNow = date + "T" + time + ".000Z";
  return dateNow;
}

// get cars data
function getCars() {
  let htmlData = "";
  data = cars.filterCarByUser();
  if (data === "" || data === undefined) {
    htmlData = "";
    app.innerHTML = htmlData;
    return;
  } else {
    for (let index = 0; index < data.length; index++) {
      let car = data[index];
      let rentCost = formatToRupiah(car.rentPerDay);
      htmlData += `
            <div class="col m-2">
                <div class="card" style="width: 18rem; height: 550px">
                <img src="${car.image}"" class="card-img-top img-fluid" alt="${car.manufacture}" style="height: 190px; object-fit: scale-down;">
                <div class="card-body" style="font-size: 14px;">
                    <p class="card-title">${car.manufacture} ${car.model}</p>
                    <p class="fw-bold">${rentCost} / hari</p>
                    <p class="card-text" style="height: 90px">${car.description}</p>
                    <div class="my-2"><i class="bi bi-people me-2"></i>${car.capacity} Orang</div>
                    <div class="my-2"><i class="bi bi-gear me-2"></i>${car.transmission}</div>
                    <div class="my-2"><i class="bi bi-calendar4 me-2"></i>${car.year}</div>
                    <a href="#" class="btn bg-button text-white w-100 mt-2 fw-bold mt-4" style="font-size: 14px;">Pilih Mobil</a>
                </div>
                </div>
            </div>
            `;
    }
    app.innerHTML = htmlData;
    if (htmlData == "") {
      alert("Mobil Tidak tersedia");
    }
  }
}
